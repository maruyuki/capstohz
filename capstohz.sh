#!/bin/sh
xmodmap -pke > ~/.Xmodmap
sed -i '1s/^/clear lock\n/' ~/.Xmodmap
sed -i -e "s/keycode  66 = Eisu_toggle Caps_Lock Eisu_toggle Caps_Lock/keycode  66 = Zenkaku_Hankaku Kanji Zenkaku_Hankaku Kanji Zenkaku_Hankaku Kanji/" ~/.Xmodmap
